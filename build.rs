//! Ensure git information is available during build
use std::error::Error;
use vergen_gitcl::{Emitter, GitclBuilder};

fn main() -> Result<(), Box<dyn Error>> {
    Ok(Emitter::new()
        .add_instructions(
            &GitclBuilder::default()
                .commit_timestamp(true)
                .sha(false)
                .describe(true, false, None)
                .build()?,
        )?
        .fail_on_error()
        .emit()?)
}
