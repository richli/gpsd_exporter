# `gpsd` exporter for Prometheus

[![dependency status](https://deps.rs/repo/gitlab/richli/gpsd_exporter/status.svg)](https://deps.rs/repo/gitlab/richli/gpsd_exporter)

A simple exporter for Prometheus of metrics from the `gpsd` service.

## Overview

This is a very simple Rust program. The [`gpsd`](https://gpsd.gitlab.io/gpsd/)
service running on `localhost:2947/tcp` is connected to and various metrics
collected. For [Prometheus](https://prometheus.io/), this binds to
`[::]:9700/tcp` and listens for connections. [`axum`](https://docs.rs/axum) is
used as the HTTP server.

## Usage

Although this queries `gpsd`, it is not linked with `libgps`, it merely
communicates via TCP connections to `localhost`.

Build using `cargo` and run it:
```bash
cargo build --release
./target/release/gpsd_exporter
# Or, cargo run --release
```

It runs until interrupted. However, it's feasible to use systemd to run it via
socket-activation. Additionally, it notifies systemd of its status, sends
watchdog keep-alive pings, and sends output to `journald`.

For example, in this mode:
```bash
sudo cp -t "/usr/local/bin" ./target/release/gpsd_exporter
sudo cp -t "/etc/systemd/system" ./data/gpsd_exporter.{service,socket}
sudo systemctl enable --now gpsd_exporter.socket
```

### Testing socket-activation

If it's not running within a systemd service, the socket-activation can still be
tested using:

```bash
cargo build && systemd-socket-activate -l 9700 ./target/debug/gpsd_exporter
```

## Cross-compilation

The primary intent is to cross-compile on an x86_64 host for a Raspberry Pi 3
target. See some [instructions here](https://github.com/japaric/rust-cross).
Condensing it, these are some instructions to set that up on Debian:

```bash
# Install the rust stdlib for this target
rustup target add aarch64-unknown-linux-gnu
# On Debian, install the gcc toolchain for this target. We only need the linker, ld.
sudo apt install gcc-aarch64-linux-gnu
# For this project, tell cargo which linker to use. (Note that the target tuple for Rust and GCC are slightly different!)
mkdir .cargo
cat > .cargo/config.toml <<EOF
[target.aarch64-unknown-linux-gnu]
linker = "aarch64-linux-gnu-gcc"
EOF
# Build for the target
cargo build --target=aarch64-unknown-linux-gnu
```

## License

This work is licensed under the [MIT License](LICENSE).
