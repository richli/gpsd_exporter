//! Prometheus exporter for gpsd
//!
//! This is a bridge to connect to [`gpsd`](https://gpsd.gitlab.io/gpsd/) and
//! present metrics that [Prometheus](https://prometheus.io/) can scrape from.
//! This connects to the `gpsd` service via TCP, by default `localhost:2947`,
//! and updates the metrics. It listens to a TCP socket, by default `[::]:9700`,
//! which is served by the [`hyper`](https://docs.rs/hyper) HTTP server.

use axum::{middleware, routing::get, Router};
use libsystemd::daemon::{self, NotifyState};
use std::{error::Error, net, sync::Arc};
use tokio::{sync::oneshot, task};
use tower_http::compression::CompressionLayer;
use tracing::{debug, error, info};

mod config;
mod gpsd;
mod metrics;
mod server;

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), Box<dyn Error>> {
    let prog_opts = config::ProgramOptions::parse_args();
    prog_opts.init_logging();

    // Set up Prometheus metrics
    let (registry, proc_metrics, gpsd_metrics) = metrics::init_metrics();

    // Connect to gpsd in a background thread and start watching its data
    let gpsd_connection =
        net::TcpStream::connect(prog_opts.gpsd_addr.as_slice()).unwrap_or_else(|e| {
            error!(
                "Cannot connect to gpsd on {:?}: {}",
                &prog_opts.gpsd_addr, e
            );
            std::process::exit(1);
        });
    let (gpsd_exit_sender, gpsd_exit_receiver) = oneshot::channel();
    let gpsd_thread = gpsd_metrics
        .watch_gpsd(gpsd_connection, gpsd_exit_receiver)
        .unwrap_or_else(|e| {
            error!("{}", e);
            std::process::exit(1);
        });

    let http_listener = server::prepare_listener(prog_opts.listen_addr.as_slice())?;
    http_listener.set_nonblocking(true)?;
    let http_listener = tokio::net::TcpListener::from_std(http_listener)?;

    daemon::notify(false, &[NotifyState::Status("Created TCP listener".into())])?;

    let state = Arc::new(server::ServerState::new(registry, proc_metrics));

    // Let axum manage the HTTP server
    let app = {
        let state = Arc::clone(&state);
        Router::new()
            .route("/", get(server::redirect_metrics))
            .route("/metrics", get(server::get_metrics))
            .route_layer(middleware::from_fn_with_state(
                Arc::clone(&state),
                server::count_requests,
            ))
            .layer(CompressionLayer::new())
            .with_state(state)
    };

    let http_server = axum::serve(http_listener, app).with_graceful_shutdown(async move {
        tokio::signal::ctrl_c()
            .await
            .expect("failed to set SIGINT handler");

        daemon::notify(false, &[NotifyState::Stopping]).unwrap();
        info!("Stopping due to SIGINT");
        gpsd_exit_sender
            .send(())
            .expect("couldn't send exit message to gpsd-watching thread");
    });

    // Create a watchdog task, if systemd requests a watchdog timer
    task::spawn(async {
        if let Some(watchdog_timeout) = daemon::watchdog_enabled(false) {
            // The `sd-daemon.h` documentation recommends setting the interval
            // to half of what's requested.
            let watchdog_interval = watchdog_timeout / 2;
            debug!(
                "Setting up systemd watchdog ping every {} s",
                watchdog_interval.as_secs_f32()
            );
            let mut interval = tokio::time::interval(watchdog_interval);
            loop {
                interval.tick().await;
                debug!("Sending systemd watchdog ping");
                daemon::notify(false, &[NotifyState::Watchdog]).unwrap();
            }
        }
    });

    // Create a periodic task to update the status message with the number of
    // requests served
    task::spawn(async move {
        let mut interval = tokio::time::interval(std::time::Duration::from_secs(30));
        loop {
            interval.tick().await;
            let num_requests = state.num_requests();
            daemon::notify(
                false,
                &[NotifyState::Status(format!(
                    "{num_requests} HTTP requests for metrics served"
                ))],
            )
            .unwrap();
        }
    });

    // We're all loaded, so notify systemd that the start-up phase is finished
    daemon::notify(
        false,
        &[
            NotifyState::Ready,
            NotifyState::Status("Ready to serve HTTP requests".into()),
        ],
    )?;

    // Everything is ready, just let the HTTP server run and once it finishes
    // (due to graceful shutdown) then join the gpsd-watching thread, which
    // should be notified to exit.
    http_server.await?;
    gpsd_thread.join().unwrap();
    Ok(())
}
