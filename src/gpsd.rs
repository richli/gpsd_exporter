//! Routines for reading data from gpsd

use std::thread::JoinHandle;

use crate::metrics::{GpsdMetrics, SatelliteUsed};
use gpsd_proto::{GpsdError, Mode, ResponseData};
use tokio::sync::oneshot::{self, error::TryRecvError};
use tracing::{debug, info, trace, warn};

impl GpsdMetrics {
    /// Sit on the connection to gpsd and watch for data of interest.
    ///
    /// It first tries to connect with gpsd and if it can't, it quickly returns
    /// a `GpsdError`. Otherwise, it spawns a background thread, returning its
    /// `JoinHandle`.
    ///
    /// The background thread should loop forever in normal conditions but could
    /// exit early if there's some issue receiving the data from gpsd (e.g.,
    /// early termination). In order to signal the background thread to quit, a
    /// oneshot channel is checked on each iteration to see if it's ready.
    pub fn watch_gpsd(
        self,
        gpsd_connection: std::net::TcpStream,
        mut exit_notification: oneshot::Receiver<()>,
    ) -> Result<JoinHandle<()>, GpsdError> {
        debug!(
            "Connecting to gpsd via {}",
            gpsd_connection.peer_addr().unwrap()
        );

        // Note that the `net` and `io` modules here are from std, not
        // tokio. This is what the `gpsd_proto` crate supports.
        let mut writer = std::io::BufWriter::new(gpsd_connection.try_clone()?);
        let mut reader = std::io::BufReader::new(gpsd_connection);
        gpsd_proto::handshake(&mut reader, &mut writer)?;
        info!("Handshake established with gpsd");

        // At this point no more writing is required for the GPSD connection
        drop(writer);

        Ok(std::thread::spawn(move || {
            loop {
                if let Ok(_) | Err(TryRecvError::Closed) = exit_notification.try_recv() {
                    break;
                }

                let response = gpsd_proto::get_data(&mut reader);
                // Unwrap the response but for JSON errors (which seem to be more common
                // lately) simply warn and try again
                let data = match response {
                    Err(GpsdError::JsonError(e)) => {
                        warn!("gpsd json error: {}", e);
                        continue;
                    }
                    Err(e) => {
                        panic!("gpsd_proto::get_data failed: {e}");
                    }
                    Ok(data) => data,
                };

                trace!("gpsd data: {:?}", data);
                match data {
                    ResponseData::Sky(sky) => {
                        // If any DOP is missing, set it to NaN
                        self.xdop.set(sky.xdop.map_or(std::f64::NAN, f64::from));
                        self.ydop.set(sky.ydop.map_or(std::f64::NAN, f64::from));
                        self.vdop.set(sky.vdop.map_or(std::f64::NAN, f64::from));
                        self.hdop.set(sky.hdop.map_or(std::f64::NAN, f64::from));
                        self.tdop.set(sky.tdop.map_or(std::f64::NAN, f64::from));
                        self.pdop.set(sky.pdop.map_or(std::f64::NAN, f64::from));
                        self.gdop.set(sky.gdop.map_or(std::f64::NAN, f64::from));

                        if let Some(satellites) = sky.satellites {
                            let number_satellites_used =
                                satellites.iter().filter(|satellite| satellite.used).count();
                            let number_satellites_unused =
                                satellites.len() - number_satellites_used;
                            self.number_satellites
                                .get_or_create(&SatelliteUsed::used())
                                .set(number_satellites_used as i64);
                            self.number_satellites
                                .get_or_create(&SatelliteUsed::unused())
                                .set(number_satellites_unused as i64);

                            for sat in satellites {
                                if sat.used {
                                    if let Some(el) = sat.el {
                                        self.elevation.observe(el.into());
                                    }
                                    if let Some(az) = sat.az {
                                        self.azimuth.observe(az.into());
                                    }
                                    if let Some(ss) = sat.ss {
                                        self.snr.observe(ss.into());
                                    }
                                }
                            }
                        }
                    }
                    ResponseData::Tpv(tpv) => {
                        match (tpv.mode, tpv.status) {
                            (Mode::NoFix, _) => self.mode.set(0),
                            (Mode::Fix2d, None) => self.mode.set(1),
                            (Mode::Fix2d, Some(2)) => self.mode.set(2),
                            (Mode::Fix3d, None) => self.mode.set(3),
                            (Mode::Fix3d, Some(2)) => self.mode.set(4),
                            (_, Some(_)) => panic!("unexpected TPV status value"),
                        };
                        // If any value is missing, set it to NaN
                        self.lat.set(tpv.lat.unwrap_or(std::f64::NAN));
                        self.lon.set(tpv.lon.unwrap_or(std::f64::NAN));
                        self.alt.set(tpv.alt.map_or(std::f64::NAN, f64::from));
                        self.track.set(tpv.track.map_or(std::f64::NAN, f64::from));
                        self.speed.set(tpv.speed.map_or(std::f64::NAN, f64::from));
                        self.climb.set(tpv.climb.map_or(std::f64::NAN, f64::from));
                    }
                    ResponseData::Device(_) | ResponseData::Pps(_) | ResponseData::Gst(_) => (),
                }
            }
        }))
    }
}
