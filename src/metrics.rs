//! Define all the metrics needed for gpsd

use std::sync::atomic::AtomicU64;

use prometheus_client::{
    encoding::EncodeLabelSet,
    metrics::{counter::Counter, family::Family, gauge::Gauge, histogram::Histogram, info::Info},
    registry::{Registry, Unit},
};
use tracing::trace;

#[derive(Default)]
pub(crate) struct ProcessMetrics {
    /// Total user and system CPU time in seconds
    pub cpu: Counter,
    /// Maximum number of open file descriptors
    pub max_fds: Gauge,
    /// Number of open file descriptors
    pub open_fds: Gauge,
    /// Resident memory size in bytes
    pub resident_memory: Gauge,
    /// Start time of the process since epoch in seconds
    pub start_time: Gauge,
    /// Number of OS threads in the process
    pub threads: Gauge,
    /// Virtual memory size in bytes
    pub virtual_memory: Gauge,
}

/// The metrics collected from gpsd.
pub(crate) struct GpsdMetrics {
    /// Latitude in degrees
    pub lat: Gauge<f64, AtomicU64>,
    /// Longitude in degrees
    pub lon: Gauge<f64, AtomicU64>,
    /// Altitude in meters
    pub alt: Gauge<f64, AtomicU64>,
    /// Course over ground, degrees from North
    pub track: Gauge<f64, AtomicU64>,
    /// Speed over ground, m/s
    pub speed: Gauge<f64, AtomicU64>,
    /// Climb or sink (positive/negative) rate, m/s
    pub climb: Gauge<f64, AtomicU64>,
    /// Fix mode (none, 2d, 3d)
    pub mode: Gauge,
    /// Longitudinal dilution of precision
    pub xdop: Gauge<f64, AtomicU64>,
    /// Latitudinal dilution of precision
    pub ydop: Gauge<f64, AtomicU64>,
    /// Altitude dilution of precision
    pub vdop: Gauge<f64, AtomicU64>,
    /// Horizontal dilution of precision
    pub hdop: Gauge<f64, AtomicU64>,
    /// Time dilution of precision
    pub tdop: Gauge<f64, AtomicU64>,
    /// Position dilution of precision
    pub pdop: Gauge<f64, AtomicU64>,
    /// Geometric dilution of precision
    pub gdop: Gauge<f64, AtomicU64>,
    /// Satellites in view and used
    pub number_satellites: Family<SatelliteUsed, Gauge>,
    /// Satellite elevation angles (from -90° to 90°)
    pub elevation: Histogram,
    /// Satellite azimuth angles (from 0° to 359°)
    pub azimuth: Histogram,
    /// Satellite signal-to-noise ratios (from 0 to 254 dB)
    pub snr: Histogram,
}

/// Whether a satellite is used or not
#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub(crate) struct SatelliteUsed(bool);

impl SatelliteUsed {
    /// Label for used satellites
    pub(crate) const fn used() -> Self {
        Self(true)
    }

    /// Label for unused satellites
    pub(crate) const fn unused() -> Self {
        Self(false)
    }
}

impl EncodeLabelSet for SatelliteUsed {
    fn encode(
        &self,
        encoder: prometheus_client::encoding::LabelSetEncoder<'_>,
    ) -> Result<(), std::fmt::Error> {
        let label_value = if self.0 { "yes" } else { "no" };
        EncodeLabelSet::encode(&[("used", label_value)], encoder)
    }
}

impl Default for GpsdMetrics {
    fn default() -> Self {
        let elevation_bins = [-10., 15., 30., 45., 60., 75.];
        let azimuth_bins = [
            30., 60., 90., 120., 150., 180., 210., 240., 270., 300., 330.,
        ];
        // This is approximately logarithmically spaced
        let snr_bins = [1., 2., 3., 5., 8., 12., 21., 34., 56., 93., 155.];

        Self {
            lat: Default::default(),
            lon: Default::default(),
            alt: Default::default(),
            track: Default::default(),
            speed: Default::default(),
            climb: Default::default(),
            mode: Default::default(),
            xdop: Default::default(),
            ydop: Default::default(),
            vdop: Default::default(),
            hdop: Default::default(),
            tdop: Default::default(),
            pdop: Default::default(),
            gdop: Default::default(),
            number_satellites: Default::default(),
            elevation: Histogram::new(elevation_bins.into_iter()),
            azimuth: Histogram::new(azimuth_bins.into_iter()),
            snr: Histogram::new(snr_bins.into_iter()),
        }
    }
}

/// Initialize the gpsd metrics
pub(crate) fn init_metrics() -> (Registry, ProcessMetrics, GpsdMetrics) {
    let mut registry = Registry::default();
    let proc_metrics = ProcessMetrics::default();
    let metrics = GpsdMetrics::default();

    let info = vec![
        (
            "version",
            option_env!("VERGEN_GIT_DESCRIBE").unwrap_or(env!("CARGO_PKG_VERSION")),
        ),
        (
            "commit_date",
            option_env!("VERGEN_GIT_COMMIT_TIMESTAMP").unwrap_or("Unknown"),
        ),
        ("commit", option_env!("VERGEN_GIT_SHA").unwrap_or("Unknown")),
    ];

    let degrees = Unit::Other("degrees".into());

    registry.register(
        "gpsd_exporter_build",
        "Build information for the gpsd exporter",
        Info::new(info),
    );

    /// Register a metric
    ///
    /// This reduces a bit of boilerplate by overloading a couple of functions.
    ///
    /// # Example
    /// ```
    /// // A metric without a unit
    /// register!("metric_name", "metric_help", metric);
    /// // A metric with a unit
    /// register!("metric_name", "metric_help", Unit::Meters, metric);
    /// ```
    macro_rules! register {
        ($name:literal, $help:literal, $metric:expr) => {
            registry.register($name, $help, $metric.clone());
        };
        ($name:literal, $help:literal, $unit:expr, $metric:expr) => {
            registry.register_with_unit($name, $help, $unit.clone(), $metric.clone());
        };
    }

    register!("gpsd_latitude", "Latitude in degrees", degrees, metrics.lat);
    register!(
        "gpsd_longitude",
        "Longitude in degrees",
        degrees,
        metrics.lon
    );
    register!(
        "gpsd_altitude",
        "Altitude in meters",
        Unit::Meters,
        metrics.alt
    );
    register!(
        "gpsd_track",
        "Course over ground in degrees from North",
        degrees,
        metrics.track
    );
    register!(
        "gpsd_speed_meters_per_second",
        "Speed over ground in meters per second",
        metrics.speed
    );
    register!(
        "gpsd_climb_meters_per_second",
        "Climb or sink rate in meters per second",
        metrics.climb
    );
    register!(
        "gpsd_fix",
        "Type of fix: None, 2d, 2d DGPS, 3d, or 3d DGPS",
        metrics.mode
    );
    register!(
        "gpsd_xdop",
        "Longitudinal dilution of precision",
        metrics.xdop
    );
    register!(
        "gpsd_ydop",
        "Latitudinal dilution of precision",
        metrics.ydop
    );
    register!("gpsd_vdop", "Altitude dilution of precision", metrics.vdop);
    register!(
        "gpsd_hdop",
        "Horizontal dilution of precision",
        metrics.hdop
    );
    register!("gpsd_tdop", "Time dilution of precision", metrics.tdop);
    register!("gpsd_pdop", "Position dilution of precision", metrics.pdop);
    register!("gpsd_gdop", "Geometric dilution of precision", metrics.gdop);
    register!(
        "gpsd_satellites",
        "The number of satellites tracked",
        metrics.number_satellites
    );
    register!(
        "gpsd_satellite_elevation",
        "Elevation angles of all satellites used",
        degrees,
        metrics.elevation
    );
    register!(
        "gpsd_satellite_azimuth",
        "Azimuth angles of all satellites used",
        degrees,
        metrics.azimuth
    );
    register!(
        "gpsd_satellite_snr_db",
        "Signal-to-noise ratios of all satellites used",
        metrics.snr
    );

    // Note that these should be using the proper units (bytes and seconds) but
    // I'm keeping compatible with the existing de-facto style
    register!(
        "process_cpu_seconds",
        "Total user and system CPU time spent in seconds",
        proc_metrics.cpu
    );
    register!(
        "process_max_fds",
        "Maximum number of open file descriptors",
        proc_metrics.max_fds
    );
    register!(
        "process_open_fds",
        "Number of open file descriptors",
        proc_metrics.open_fds
    );
    register!(
        "process_resident_memory_bytes",
        "Resident memory size in bytes",
        proc_metrics.resident_memory
    );
    register!(
        "process_start_time_seconds",
        "Start time of the process since unix epoch in seconds",
        proc_metrics.start_time
    );
    register!(
        "process_threads",
        "Number of OS threads in the process",
        proc_metrics.threads
    );
    register!(
        "process_virtual_memory_bytes",
        "Virtual memory size in bytes",
        proc_metrics.virtual_memory
    );

    (registry, proc_metrics, metrics)
}

impl ProcessMetrics {
    /// Update the process metrics.
    ///
    /// This just queries the information from
    /// [`/proc`](https://www.man7.org/linux/man-pages/man5/proc.5.html) using
    /// the `procfs` crate.
    pub(crate) fn collect(&self) -> procfs::ProcResult<()> {
        use procfs::{
            boot_time_secs, process::LimitValue, process::Process, ticks_per_second,
            WithCurrentSystemInfo as _,
        };

        trace!("Querying /proc for process metrics");

        let p = Process::myself()?;

        let open_fds = p.fd_count()?;
        self.open_fds.set(open_fds.try_into().unwrap());

        let limits = p.limits()?;
        let max_fds = match limits.max_open_files.soft_limit {
            LimitValue::Value(v) => v.try_into().unwrap(),
            LimitValue::Unlimited => -1,
        };
        self.max_fds.set(max_fds);

        let stat = p.stat()?;
        self.threads.set(stat.num_threads);

        let user_ticks = stat.utime;
        let sys_ticks = stat.stime;
        let cpu_time = (user_ticks + sys_ticks) / ticks_per_second();
        // Note that the `cpu_time` here is essentially a gauge since it's the
        // accumulated total, but the `Counter` API doesn't permit setting the
        // exact value, only a non-negative increment.
        let prev_cpu = self.cpu.get();
        self.cpu.inc_by(cpu_time.saturating_sub(prev_cpu));

        let boot_time_since_epoch = boot_time_secs()?;
        let seconds_since_boot = stat.starttime / ticks_per_second();
        let time_since_epoch = seconds_since_boot + boot_time_since_epoch;
        self.start_time.set(time_since_epoch.try_into().unwrap());

        self.resident_memory
            .set(stat.rss_bytes().get().try_into().unwrap());
        self.virtual_memory.set(stat.vsize.try_into().unwrap());

        Ok(())
    }
}
